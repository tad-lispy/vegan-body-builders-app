module Food exposing
    ( Food
    , balance
    , completeness
    , decoder
    )

{-| Types and functions dealing with food types
-}

import AminoAcids exposing (AminoAcids)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import List.Extra as List


{-| Represents a certain type of food (e.g. Green Peas), it's nutritional contents per 100g and standard serving in grams.
-}
type alias Food =
    { name : String
    , serving : Float
    , aminoacids : AminoAcids
    }


balance : List Food -> Maybe Float
balance foods =
    foods
        |> List.map aminoacids
        |> List.foldl1 AminoAcids.add
        |> Maybe.map AminoAcids.balance


completeness : List Food -> Maybe Float
completeness foods =
    foods
        |> List.map aminoacids
        |> List.foldl1 AminoAcids.add
        |> Maybe.map AminoAcids.completeness


{-| Calculates amounts of aminoacids per serving of a given food
-}
aminoacids : Food -> AminoAcids
aminoacids food =
    { histidine = food.aminoacids.histidine * food.serving / 100
    , isoleucine = food.aminoacids.isoleucine * food.serving / 100
    , leucine = food.aminoacids.leucine * food.serving / 100
    , lysine = food.aminoacids.lysine * food.serving / 100
    , methionine = food.aminoacids.methionine * food.serving / 100
    , phenylalanine = food.aminoacids.phenylalanine * food.serving / 100
    , threonine = food.aminoacids.threonine * food.serving / 100
    , tryptophan = food.aminoacids.tryptophan * food.serving / 100
    , valine = food.aminoacids.valine * food.serving / 100
    }


decoder : Decoder Food
decoder =
    Decode.succeed Food
        |> Decode.required "name" Decode.string
        |> Decode.required "serving" Decode.float
        |> Decode.custom AminoAcids.decoder
